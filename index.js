var express = require('express');
var chalk = require('chalk');
var debug = require('debug')('index');


//creating an instance of express
var app = express();

app.get('/', function(req,res){
   res.send('hello from my library app');
})

app.listen(3000,function(){

console.log(`listening on port number" ${chalk.green('3000')}`);

});
